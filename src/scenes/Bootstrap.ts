import Phaser from 'phaser';

/**
 * Bootstrap.
 *
 * @extends {Phaser.Scene}
 */
class Bootstrap extends Phaser.Scene {
  constructor() {
    super('bootstrap');
  }

  init() {}

  preload() {
    // This Loads my assets created with texture packer
    // this.load.multiatlas('tankers', 'assets/tanker-game.json', 'assets')
    this.load.image('rectangleFilled', 'assets/rectangleFilled.png');
    this.load.image('rectangleShell', 'assets/rectangleShell.png');
  }

  create() {
    this.createNewGame();
  }

  update() {}

  private createNewGame() {
    // this launches the game scene
    this.scene.launch('game');
  }
}

export default Bootstrap;
