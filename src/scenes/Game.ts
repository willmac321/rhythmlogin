import Phaser from 'phaser';
//https://blog.ourcade.co/posts/2023/building-phaser-3-ecs-game-with-reactjs/

/**
 * The Game component
 *
 * @extends {Phaser.Scene}
 */
class Game extends Phaser.Scene {

  keyBox?: Phaser.Types.Physics.Arcade.ImageWithStaticBody

  letter?: Phaser.Types.Physics.Arcade.SpriteWithDynamicBody;

  keyEvent: KeyboardEvent | null = null;

  velocity?: number;

  startPosition: { x: number; y: number } = { x: 400, y: 0 };

  constructor() {
    super('game');
  }

  init() {}

  create() {
    this.resetSprite();
    this.keyBox = this.physics.add.staticImage(400, 550, 'rectangleShell');

    this.input.keyboard?.on('keydown', (event: KeyboardEvent) => {this.keyEvent = event});
  }

  update(time: number, delta: number): void {
    if( this.isMatchLetterAndCollision() ){
      console.log('yes');
      this.resetSprite();
    } else if (this.letter && this.letter.y > 650) {
      this.resetSprite();
    }
  }
  
  resetSprite(): void {
    this.letter?.disableBody();
    const oldLetter = this.letter;

    this.velocity = ((Math.random() * 600 + 1) % 600) + 600;
    this.letter = this.physics.add
      .sprite(this.startPosition.x, this.startPosition.y, 'rectangleFilled');
    this.letter.setVelocityY(this.velocity);

    setTimeout(()=>{oldLetter?.destroy()}, 100);;
  }

  isMatchLetterAndCollision(): boolean {
    if (this.keyEvent?.key === ' '){
      this.keyEvent = null;
      return true;
    }
      this.keyEvent = null;
    return false;
  }

}

export default Game;
