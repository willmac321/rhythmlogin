import { useState } from 'react';
import './App.css';
import './Phaser.ts';

function App() {
  const [count, setCount] = useState(0);

  return (
    <div
      id="phaser-container"
      style={{ height: '100%', width: '100%' }}
    ></div>
  );
}

export default App;
